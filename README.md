# TRK

EPAM FE LAB nodejs hw №3

## Application dependencies

- [Express.js](https://expressjs.com)

## Environment dependencies

- [Node.js](nodejs.org)

## Usage

- `npm install && npm run build`: build the project
- `npm run start:dev`: start development server watching changes in ./src
- `npm start`: start production server running the compiled app in ./lib
