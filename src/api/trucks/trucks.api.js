const express = require('express');
const trucksController = require('./trucksController');
const {authMiddleware} = require('../auth/authMiddleware');

const router = express.Router();

router.get('/', authMiddleware, trucksController.get);

router.get('/:id', authMiddleware, trucksController.getTruck);

router.put('/:id', authMiddleware, trucksController.update);

router.post('/', authMiddleware, trucksController.add);

router.post('/:id/assign', authMiddleware, trucksController.assign);

router.delete('/:id', authMiddleware, trucksController.delete);

module.exports = router;
