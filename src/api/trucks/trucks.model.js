const Truck = require('./trucks.schema');

/**
 * @param {string} userId
 * @param {string} type
 * @return {Promise<void>}
 */
const addUsersTruck = async (userId, type) => {
  const truck = new Truck({created_by: userId, type});
  await truck.save();
};

/**
 * @param {string} userId
 * @return {Promise<Array | null>}
 */
const getUsersTrucks = async (userId) => Truck.find({created_by: userId});

/**
 * @param {string} userId
 * @param {string} truckId
 * @return {Promise<Query<Document | null, Document>>}
 */
const getUsersTruckById = async (userId, truckId) => Truck.findOne({_id: truckId, created_by: userId},
    '_id created_by assigned_to type status created_date');

/**
 * @param {string} userId
 * @param {string} truckId
 * @return {Promise<void>}
 */
const deleteUsersTruck = async (userId, truckId) => Truck.findOneAndDelete({_id: truckId, created_by: userId});

/**
 * @param {string} userId
 * @param {string} truckId
 * @param {string} type
 * @return {Promise<Query<Document | null, Document>>}
 */
const updateTruckType = async (userId, truckId, type) => {
  const truck = await Truck.findOneAndUpdate({
    _id: truckId,
    created_by: userId,
    status: Truck.statuses.is,
  }, {type});
  if (!truck) throw new Error(`Truck with ID "${truckId}" not found!`);
};

/**
 * @param {string} userId
 * @param {string} truckId
 * @return {Promise<void>}
 */
const assignUserToTruck = async (userId, truckId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: userId});
  if (!truck) throw new Error(`You don't have that vehicle`);
  truck.assigned_to = userId;
  await truck.save();
};

const findTruck = async ({dimensions: {width, length, height}, payload}) => {
  const suitableTruckTypes = Object.entries(Truck.types).reduce((accum, [type, p]) => {
    if (payload < p.payload && width < p.width && length < p.length && height < p.height) {
      accum.push(type);
    }
    return accum;
  }, []);

  return Truck.findOne({
    status: {$eq: Truck.statuses.is},
    assigned_to: {$ne: null},
    type: {$in: suitableTruckTypes},
  });
};

const getActiveTruck = async (userId) => Truck.findOne({
  status: {$eq: Truck.statuses.ol},
  assigned_to: {$eq: userId},
});

module.exports = {
  addUsersTruck,
  getUsersTrucks,
  getUsersTruckById,
  updateTruckType,
  assignUserToTruck,
  deleteUsersTruck,
  findTruck,
  getActiveTruck,
};
