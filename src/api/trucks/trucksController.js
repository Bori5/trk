const model = require('./trucks.model');
const Joi = require('joi');

const joiSchema = Joi.object({
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  role: Joi.string().valid('DRIVER'),
});

module.exports = class TrucksController {
  static async get({user: {_id: userId}}, res) {
    try {
      const trucks = await model.getUsersTrucks(userId);
      res.status(200).json({trucks});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async add({user: {_id: userId, role}, body: {type}}, res) {
    try {
      await joiSchema.validateAsync({type, role});
      await model.addUsersTruck(userId, type);
      res.json({message: 'Truck created successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async getTruck({user: {_id: userId}, params: {id}}, res) {
    try {
      const truck = await model.getUsersTruckById(userId, id);
      res.status(200).json({truck});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async update({user: {_id: userId, role}, params: {id}, body: {type}}, res) {
    try {
      await joiSchema.validateAsync({type, role});
      await model.updateTruckType(userId, id, type);
      res.json({message: 'Truck details changed successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async assign({user: {_id: userId}, params: {id}}, res) {
    try {
      await model.assignUserToTruck(userId, id);
      res.status(200).json({message: 'Truck assigned successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async delete({user: {_id: userId}, params: {id}}, res) {
    try {
      await model.deleteUsersTruck(userId, id);
      res.status(200).json({message: 'Truck deleted successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }
};
