const {model, Schema} = require('mongoose');

const types = Object.freeze({
  'SPRINTER': {
    length: 300,
    width: 250,
    height: 170,
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    length: 500,
    width: 250,
    height: 170,
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    length: 700,
    width: 350,
    height: 250,
    payload: 4000,
  },
});

const statuses = Object.freeze({
  is: 'IS',
  ol: 'OL',
});

const trucksSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: Object.keys(types),
    required: true,
  },
  status: {
    type: String,
    enum: Object.values(statuses),
    default: statuses.is,
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
  },
}, {versionKey: false});
Object.assign(trucksSchema.statics, {types}, {statuses});

module.exports = model('Truck', trucksSchema);
