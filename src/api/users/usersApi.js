const {authMiddleware} = require('../auth/authMiddleware');
const userController = require('./usersController');
const express = require('express');
const router = express.Router();

router.get('/', authMiddleware, userController.get);

router.delete('/', authMiddleware, userController.delete);

router.patch('/password', authMiddleware, userController.changePassword);

module.exports = router;
