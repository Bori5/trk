const Joi = require('joi');
const Model = require('./users.model');
const authTools = require('../auth/authTools');

const joiSchema = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
});

module.exports = class UserController {
  static async get({user: {_id}}, res) {
    try {
      const user = await Model.findById(_id);
      res.status(200).json({user});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async delete({user: {_id}}, res) {
    try {
      await Model.delete(_id);
      res.status(200).json({message: 'Profile deleted successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async changePassword({user: {_id}, body: {oldPassword, newPassword}}, res) {
    try {
      await joiSchema.validateAsync({oldPassword, newPassword});
      const user = await Model.findById(_id, 'password');

      if (!(await authTools.isPasswordMatch(oldPassword, user.password))) {
        return res.status(400).json({message: 'password does not match!'});
      }

      user.password = await authTools.createPasswordHash(newPassword);
      await user.save();
      res.json({message: 'Password changed successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }
};
