const {model, Schema} = require('mongoose');

const userSchema = new Schema({
  role: {
    type: String,
    require: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    require: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
}, {versionKey: false});

module.exports = model('User', userSchema);
