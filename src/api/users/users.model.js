const User = require('./users.schema');

module.exports = {
  addUser: async (email, password, role) => {
    const user = new User({role, email, password});
    return await user.save();
  },
  findByEmail: async (email) => User.findOne({email}),
  findById: async (_id, selection = '_id email created_date') => User.findById({_id}, selection).exec(),
  delete: async (_id) => {
    const user = await User.findOneAndDelete({_id});
    if (!user) throw new Error(`User doesn't exist`);
  },
};
