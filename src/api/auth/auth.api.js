const authController = require('./authController');
const express = require('express');

const router = express.Router();

router.post('/login', authController.login);

router.post('/register', authController.register);

router.post('/forgot_password', authController.resetPassword);

module.exports = router;
