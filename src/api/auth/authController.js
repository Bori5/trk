const {findByEmail, addUser} = require('../users/users.model');
const authTool = require('./authTools');
const Joi = require('joi');

const passwordPattern = new RegExp('^[a-zA-Z0-9]{3,30}$');
const loginSchema = Joi.object({
  email: Joi.string().required().email({minDomainSegments: 2, tlds: {allow: false}}),
  password: Joi.string().required().pattern(passwordPattern),
});
const registrationSchema = loginSchema.keys({
  role: Joi.string().valid('SHIPPER', 'DRIVER'),
});

module.exports = class AuthController {
  static async register({body: {email, password, role}}, res) {
    try {
      await registrationSchema.validateAsync({email, password, role});
      await addUser(email, await authTool.createPasswordHash(password), role);
      res.json({message: 'Profile created successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async login({body: {email, password}}, res) {
    try {
      await loginSchema.validateAsync({email, password});
      const user = await findByEmail(email);

      if (!user || !(await authTool.isPasswordMatch(password, user.password))) {
        return res.status(400).json({message: `Wrong email or password!`});
      }

      res.json({jwt_token: await authTool.createUserToken(user)});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  /**
   * Stub method
   * @param {Object} email
   * @param {Express.response} res
   * @return {Promise<void>}
   */
  static async resetPassword({body: {email}}, res) {
    res.json({message: 'New password sent to your email address'});
  }
};
