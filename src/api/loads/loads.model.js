const Load = require('./loads.schema');
const logger = require('./logs.model');
const truck = require('../trucks/trucks.model');

const populateLogs = async (doc) => doc.populate('logs', 'time message -_id');

const addUsersLoad = async (userId, obj) => {
  const load = new Load(Object.assign({created_by: userId}, obj));
  await load.save();
};

const getUsersLoadById = async (userId, loadId) => populateLogs(Load.findOne({_id: loadId, created_by: userId}));

const getDrivesActiveLoad = async (userId) => populateLogs(Load.findOne({
  assigned_to: userId,
  status: Load.statuses.assigned,
}));

const getShippersActiveLoad = async (userId, loadId) => {
  const load = await Load.findOne({
    created_by: userId,
    status: Load.statuses.assigned}).lean().populate('logs', 'time message -_id');

  load.truck = await truck.getActiveTruck(load.assigned_to).then((doc) => doc.toObject());

  return load;
};

const getAssigned = async (userId) => populateLogs(Load.findOne({
  status: {$eq: Load.statuses.assigned},
  assigned_to: {$eq: userId},
}));

/**
 * @param {string} id
 * @return {Promise<void>}
 */
const postLoad = async (id) => {
  const load = await Load.findByIdAndUpdate(id, {status: Load.statuses.posted}).exec();
  await logger.add(load, 'Load status changed to "POSTED"');
};

/**
 * @param {string} id
 * @return {Promise<void>}
 */
const unPostLoad = async (id) => {
  const load = await Load.findByIdAndUpdate(id, {status: Load.statuses.new}).exec();
  await logger.add(load, 'Load status changed to "NEW"');
};

/**
 * @param {string} userId
 * @param {string} loadId
 * @param {Object} data
 * @return {Promise<Document | null>}
 */
const updateUsersLoad = async (userId, loadId, data) => Load.findOneAndUpdate({_id: loadId, created_by: userId}, data)
    .exec();

const deleteUsersLoad = async (userId, loadId) => {
  const loadDoc = await Load.findOneAndDelete({_id: loadId, created_by: userId});
  if (!loadDoc) throw new Error('Load doesn\'t exist');
};

const filterShippersLoads = async (userId, status, limit, offset) => populateLogs(Load.find({
  created_by: userId,
  status,
}, {}, {
  limit: +limit,
  skip: +offset,
  sort: {created_date: -1},
}));

const filterDriversLoads = async (userId, limit, offset) => populateLogs(Load.find({
  assigned_to: userId, status: {$in: [Load.statuses.assigned, Load.statuses.shipped]}}, {}, {
  limit: +limit,
  skip: +offset,
  sort: {created_date: -1},
}));

module.exports = {
  addUsersLoad,
  getAssigned,
  getUsersLoadById,
  getDrivesActiveLoad,
  getShippersActiveLoad,
  postLoad,
  unPostLoad,
  updateUsersLoad,
  deleteUsersLoad,
  filterShippersLoads,
  filterDriversLoads,
};
