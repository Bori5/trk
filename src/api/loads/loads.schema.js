const {model, Schema} = require('mongoose');

const statuses = Object.freeze({
  new: 'NEW',
  posted: 'POSTED',
  assigned: 'ASSIGNED',
  shipped: 'SHIPPED',
});

const states = Object.freeze({
  _: 'Not processed',
  A: 'En route to Pick Up',
  B: 'Arrived to Pick Up',
  C: 'En route to delivery',
  D: 'Arrived to delivery',
});

const loadSchema = new Schema({
  created_by: {
    type: String,
    require: true,
  },
  assigned_to: {
    type: String,
    require: true,
    default: '',
  },
  status: {
    type: String,
    enum: Object.values(statuses),
    require: true,
    default: statuses.new,
  },
  state: {
    type: String,
    enum: Object.values(states),
    require: true,
    default: states._,
  },
  name: {
    type: String,
    require: true,
  },
  payload: {
    type: Number,
    require: true,
  },
  pickup_address: {
    type: String,
    require: true,
  },
  delivery_address: {
    type: String,
    require: true,
  },
  dimensions: {
    type: Object,
    required: true,
    width: {
      type: Number,
      require: true,
    },
    length: {
      type: Number,
      require: true,
    },
    height: {
      type: Number,
      require: true,
    },
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  logs: [{type: Schema.Types.ObjectId, ref: 'Log'}],
}, {versionKey: false});
Object.assign(loadSchema.statics, {statuses}, {states});

module.exports = model('Load', loadSchema);
