const express = require('express');
const loads = require('./loadsController');
const {authMiddleware} = require('../auth/authMiddleware');

const router = express.Router();

router.get('/', authMiddleware, loads.get);

router.post('/', authMiddleware, loads.add);

router.get('/active', authMiddleware, loads.getActiveLoad);

router.patch('/active/state', authMiddleware, loads.toggleState);

router.get('/:id', authMiddleware, loads.getLoad);

router.put('/:id', authMiddleware, loads.update);

router.delete('/:id', authMiddleware, loads.delete);

router.post('/:id/post', authMiddleware, loads.post);

router.get('/:id/shipping_info', authMiddleware, loads.info);

module.exports = router;
