const trucksModel = require('../trucks/trucks.model');
const Truck = require('../trucks/trucks.schema');
const loadsModel = require('./loads.model');
const Load = require('./loads.schema');
const logger = require('./logs.model');

module.exports = class Coordinator {
  static async assign(load) {
    const [, truck] = await Promise.all([await loadsModel.postLoad(load._id), trucksModel.findTruck(load)]);
    if (!truck) {
      await loadsModel.unPostLoad(load._id);
      return false;
    }
    truck.status = Truck.statuses.ol;
    load.status = Load.statuses.assigned;
    load.state = Load.states.A;
    load.assigned_to = truck.created_by;
    await Promise.all([
      logger.add(load, `Load assigned to driver with id: ${truck.created_by}`), // logger will call load.save()
      truck.save(),
    ]);

    return true;
  }
  static async promoteLoadStatus(driverId) {
    const load = await loadsModel.getAssigned(driverId);

    switch (load.state) {
      case Load.states.A:
        load.state = Load.states.B;
        break;
      case Load.states.B:
        load.state = Load.states.C;
        break;
      case Load.states.C:
        load.state = Load.states.D;
        load.status = Load.statuses.shipped;
        const truck = await trucksModel.getActiveTruck(driverId);
        truck.status = Truck.statuses.is;
        await truck.save();
        break;
    }

    await logger.add(load, `Loads state has been changed to: ${load.state}, load status: ${load.status}`);
    return load.state;
  }
};
