const model = require('./loads.model');
const Joi = require('joi');
const Coordinator = require('./coordinator');

const userSchema = Joi.object({role: Joi.string().valid('SHIPPER')});

module.exports = class LoadsController {
  static async get({user: {_id: userId, role}, query: {status, limit, offset}}, res) {
    try {
      const loads = role === 'SHIPPER' ?
        await model.filterShippersLoads(userId, status, limit, offset) :
        await model.filterDriversLoads(userId, limit, offset);
      res.status(200).json({loads});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async add({user: {_id: userId, role}, body}, res) {
    try {
      await userSchema.validateAsync({role});
      await model.addUsersLoad(userId, body);
      res.json({message: 'Load created successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async getLoad({user: {_id: userId}, params: {id}}, res) {
    try {
      const load = await model.getUsersLoadById(userId, id);
      res.status(200).json({load});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async getActiveLoad({user: {_id: userId}}, res) {
    try {
      const load = await model.getDrivesActiveLoad(userId);
      res.status(200).json({load});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async update({user: {_id: userId, role}, params: {id}, body}, res) {
    try {
      await userSchema.validateAsync({role});
      await model.updateUsersLoad(userId, id, body);
      res.json({message: 'Load details changed successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async toggleState({user: {_id: userId}}, res) {
    try {
      const state = await Coordinator.promoteLoadStatus(userId);
      res.status(200).json({message: `Load state changed to "${state}"`});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async delete({user: {_id: userId}, params: {id}}, res) {
    try {
      await model.deleteUsersLoad(userId, id);
      res.status(200).json({message: 'Load deleted successfully'});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async post({user: {_id: userId}, params: {id}}, res) {
    try {
      const load = await model.getUsersLoadById(userId, id);
      const driver = await Coordinator.assign(load);
      res.status(200).json({
        'message': 'Load posted successfully',
        'driver_found': driver,
      });
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }

  static async info({user: {_id: userId, role}, params: {id}}, res) {
    try {
      await userSchema.validateAsync({role});
      const load = await model.getShippersActiveLoad(userId, id);
      res.status(200).json({load});
    } catch (e) {
      res.status(400).json({message: e.message});
    }
  }
};
