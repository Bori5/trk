const {model, Schema} = require('mongoose');

const logSchema = new Schema({
  loadId: {type: Schema.Types.ObjectId, ref: 'Load'},
  message: {
    type: String,
    require: true,
  },
  time: {
    type: Date,
    default: new Date().toISOString(),
  },
}, {versionKey: false});

module.exports = model('Log', logSchema);
