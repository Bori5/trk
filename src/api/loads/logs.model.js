const Log = require('./logs.schema');

/**
 * @param {Document} load
 * @param {string} message
 * @return {Promise<void>}
 */
const add = async (load, message) => {
  const log = new Log({loadId: load._id, message});
  load.logs.push(log);
  await Promise.all([log.save(), load.save()]);
};

/**
 * @param {string} loadId
 * @return {Promise<Query<Array<Document>, Document>>}
 */
const getLogs = async (loadId) => Log.find({loadId}, '-_id message time');

module.exports = {
  add,
  getLogs,
};
