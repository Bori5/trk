async function getData(url = '', options = {}) {
  const response = await fetch(url, Object.assign({
    method: 'GET',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
  }, options));
  return await response.json();
}

async function postData(url = '', data, options = {}) {
  const response = await fetch(url, Object.assign({
    method: 'POST',
    credentials: 'same-origin',
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: data,
  }, options));
  return await response.json();
}

const createElement = (name, attributes, ...children) => {
  const node = document.createElement(name);
  for (const attr in attributes) {
    if (attributes.hasOwnProperty(attr)) {
      node.setAttribute(attr, attributes[attr]);
    }
  }
  for (const child of children) {
    if (child) {
      if (typeof child !== 'string') node.appendChild(child);
      else node.appendChild(document.createTextNode(child));
    }
  }

  return node;
};

const formToObject = (form) => Array.from(new FormData(form)).reduce((acc, [key, value]) => (acc[key]=value, acc), {});

export {
  getData,
  postData,
  createElement,
  formToObject,
};
