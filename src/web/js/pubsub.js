const pubSub = () => {
  const events = Object.create(null);

  return {
    on(event, cb) {
      if (!events[event]) {
        events[event] = [];
        events[event].push(cb);
      } else {
        if (events[event].indexOf(cb) === -1) {
          events[event].push(cb);
        }
      }
    },
    off(event, cb) {
      const subs = events[event];
      if (!subs || !subs.length) return;

      const index = subs.indexOf(cb);
      if (index !== -1) {
        subs.splice(index, 1);
      }
    },
    emit(event, ...load) {
      const subs = events[event];
      if (!subs || !subs.length) return;

      for (const sub of subs) {
        sub.call(null, ...load);
      }
    },
  };
};

export default pubSub;
