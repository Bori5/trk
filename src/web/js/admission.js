const ACTIVE_TAB_CLASSNAME = 'admission__tab_active';

export default class Admission {
  constructor(element) {
    this.DOM = {element};
    this.DOM.header = element.querySelector('.admission__header');
    this.DOM.triggers = element.querySelectorAll('[data-trigger]');
    this.initEvents();
  }
  initEvents() {
    this._onTabClick = (e) => {
      e.preventDefault();
      const tab = e.target.closest('.admission__tab');
      if (tab.classList.contains(ACTIVE_TAB_CLASSNAME)) return;
      const currentTab = this.DOM.header.querySelector('.' + ACTIVE_TAB_CLASSNAME);
      currentTab.classList.remove(ACTIVE_TAB_CLASSNAME);
      e.target.classList.add(ACTIVE_TAB_CLASSNAME);
      this.changePlate(e.target.dataset.type);
    };
    this._onTriggerClick = (e) => {
      e.preventDefault();
      this.changePlate(e.currentTarget.dataset.trigger);
    };

    this.DOM.header.addEventListener('click', this._onTabClick);
    this.DOM.triggers.forEach((trigger) => trigger.addEventListener('click', this._onTriggerClick));
  }
  changePlate(type) {
    this.DOM.element.dataset.active = type;
  }
  get formSignIn() {
    return this.DOM.element.querySelector('#sign-in');
  }
  get formSignUp() {
    return this.DOM.element.querySelector('#sign-up');
  }
  get formReset() {
    return this.DOM.element.querySelector('#reset-password');
  }
  get element() {
    return this.DOM.element;
  }
}
