import '../scss/styles.scss';
import '../scss/admission.scss';
import appView from './appView.js';
import notesProvider from './notesProvider.js';
import {getData, postData, formToObject} from './utils.js';

// headers: {'Content-Type': 'application/x-www-form-urlencoded'},

const app = (model, view) => {
  const init = () => {
    view.init();
    initEvents();
    readAuthInfo();
  };

  const initEvents = () => {
    view.on('signin', onLoginRequest);
    view.on('signup', onRegisterRequest);
    view.on('logout', onLogOut);
  };

  const readAuthInfo = () => {
    const token = localStorage.getItem('authToken');
    if (token) {
      getData('/api/users/me', {headers: {'Authorization': `JWT ${token}`}})
          .then((r) => {
            console.log(r);
          });
    } else {
      view.showAdmission();
    }
  };

  const onLoginRequest = (event) => {
    event.preventDefault();
    const form = event.target;

    postData(form.action, formToObject(form))
        .then((r) => {
          console.log(r);
        });
  };

  const onRegisterRequest = (event) => {
    event.preventDefault();

    const form = event.target;

    postData(form.action, formToObject(form))
        .then((r) => {
          console.log(r);
        });
  };

  const onLogOut = (obj) => {
    localStorage.removeItem('authToken');
    location.reload();
  };

  return {init};
};

app(notesProvider(), appView()).init();
