import pubSub from './pubsub.js';
import Admission from "./admission.js";

const appView = () => {
  const DOM = Object.create(null);

  const context = {
    init: () => {
      DOM.username = document.getElementById('username');
      DOM.admission = new Admission(document.querySelector('.admission'));
      DOM.btnLogOut = document.getElementById('btn_logout');

      initEvents();
    },
    setUser: (value) => {
      DOM.username.innerHTML = value;
      DOM.btnLogOut.classList.remove('hidden');
    },
    showAdmission() {
      DOM.admission.element.parentElement.classList.remove('hidden');
    },
    hideAdmission() {
      DOM.admission.element.parentElement.classList.add('hidden');
    },
  };

  const initEvents = () => {
    DOM.btnLogOut.addEventListener('click', onLogOutClick);
  };

  const onLogOutClick = (e) => {
    context.emit('logout');
  };

  const onFormSignInSubmit = (e) => {
    context.emit('signin', e);
  };
  const onFormSignUpRSubmit = (e) => {
    context.emit('signup', e);
  };

  return Object.freeze(Object.assign(context, pubSub()));
};

export default appView;
