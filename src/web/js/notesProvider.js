import pubSub from './pubsub.js';
import {getData, postData} from './utils.js';

const notesProvider = () => {
  let authorization = '';
  const context = Object.create(null);

  context.setToken = (token) => authorization = token;

  context.get = (offset, limit) => {
    getData(`/api/notes?offset=${offset}&limit=${limit}`, {headers: {'Authorization': authorization}})
        .then((r) => {
          context.emmit('notes', r);
        });
  };

  context.getById = (id) => {
    getData('/api/trucks/' + id, {
      method: 'PATCH',
      headers: {'Authorization': authorization},
    }).then((r) => {
      context.emmit('note');
    });
  };

  context.addNote = (text) => {
    postData('/api/trucks', {text}, {headers: {'Authorization': authorization}}).then((R) => {
      if (R.success) {
        context.emit('changed');
      }
    });
  };

  context.editNote = (id, updatedText) => {};

  context.toggleNote = (id) => {
    getData('/api/trucks/' + id, {
      method: 'PATCH',
      headers: {'Authorization': authorization},
    }).then((r) => {
      context.emit('changed');
    });
  };

  context.deleteNote = (id) => {};

  return Object.freeze(Object.assign(context, pubSub()));
};

export default notesProvider;
